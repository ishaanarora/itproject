db = DAL('sqlite://webform.sqlite')
db.define_table('details',
	Field('First_name',requires = IS_NOT_EMPTY()),
	Field('Last_name',requires = IS_NOT_EMPTY()),
	Field('Username', requires = IS_NOT_EMPTY()),
	Field('Email',requires = [IS_NOT_EMPTY(),IS_NOT_IN_DB(db,'details.Email')]),
	Field('Password','password',requires = IS_NOT_EMPTY()),
	Field('quizes','list:integer')
	)
db.define_table('login',
	Field('Email',requires = [IS_NOT_EMPTY(),IS_IN_DB(db,'details.Email')]),	
	Field('Password','password',requires = IS_NOT_EMPTY()),
	Field('session_id'),
	Field('session_status'))
db.define_table('quiz_session',
	Field('quiz_session'),
	Field('Email',requires = [IS_NOT_EMPTY(),IS_IN_DB(db,'details.Email')]),
	Field('status'),
	Field('given_answers','list:string'),
	Field('questions','list:integer'),
	Field('Score','integer'))

db.define_table('question_bank',
	Field('level'),
	Field('question'),
	Field('option_a'),
	Field('option_b'),
	Field('option_c'),
	Field('option_d'),
	Field('answer'))
db.define_table('level2',
	Field('question'),
	Field('option_a'),
	Field('option_b'),
	Field('option_c'),
	Field('option_d'),
	Field('answer'))
