def register():
	record = db.details(request.args(0))
	form = SQLFORM(db.details,record)
	if form.accepts(request,session):
		response.title = "You have successfully registered"
		db(db.details.id == form.vars.id).update(quizes = [])
	elif form.errors:
		response.title = "Sorry You need to register again"
	else:
		response.title = "welcome"
	return dict(form = form)

def all_records():
      grid = SQLFORM.grid(db.question_bank,user_signature=False)
      return dict(grid = grid)

def login():
	import random
	record = db.login(request.args(0))
	form = SQLFORM(db.login,record)
	if form.accepts(request,session):
	#	session_id = request.args[0]		
		if(form.vars.Password == db(db.details.Email == form.vars.Email).select()[0].Password):				
				response.title = "you have successfully logged in"
				x = str(random.randint(1000000,10000000))
				db(db.login.id == form.vars.id).update(session_id = x, session_status = 1)
				redirect(URL('user/' + x))
		else:
			response.title = "Sorry wrong password"
			name = "Stranger"
	elif form.errors:
		response.title = "Sorry Wrong UserName Or Password"
		name = "Guest"
	else:
		response.title = "please fill in your login details"
		name = "Guest"
	return dict(form = form,name = name)
		
def user():
	session_id = request.args[0]
	r = check(session_id)
	if(r == '0'):
		response.title = "hello world"
		redirect(URL('not_logged_in'))
	name = get_name(session_id)
	response.title = "Welcome " + name
	return dict(name = name,session_id = session_id)

def get_name(session_id):
	return db(db.login.session_id == session_id).select()[0].Email

"""This function checks if the given session is valid or not"""
def check(session_id):
	x = db(db.login.session_id == session_id).select()
	if(len(x) == 0):
		return 0
	else :
		return x[0].session_status
def start_quiz():
	import random
	session_id = request.args[0]
	x = str(random.randint(1000000,2000000))
	r = check(session_id)
	if(r == '0'):
		response.title = "hello world"
		redirect(URL('not_logged_in'))
	name = get_name(session_id)
	quiz_id = db.quiz_session.insert(quiz_session = x, Email = name, status = 1, Score = 0,questions = [],given_answers = [0,0,0,0,0,0,0,0,0,0])
	user = db(db.details.Email == name).select()[0]
	quizes = user.quizes
	quizes = quizes + [quiz_id]
	db(db.details.Email == name).update(quizes = quizes)
	redirect(URL('quiz/'+session_id+'/'+str(quiz_id)))
	return dict()
def quiz():
	import random
	session_id = request.args[0]
	quiz_id = request.args[1]
	r = check(session_id)
	if(r == '0'):
		response.title = "hello world"
		redirect(URL('not_logged_in'))
	name = get_name(session_id)
	response.title = "Player: " + name
	answers = db(db.quiz_session.id == quiz_id).select()[0].given_answers
	if(len(db(db.quiz_session.id == quiz_id).select()[0].questions) == 0):
		questions = []
		option1 = []
		option2 = []
		option3 = []
		option4 = []
		for i in range(1,11):
			question_list = db(db.question_bank.level == i).select()
			length = len(question_list)
			x = int(random.randint(0,length-1))
			y = db(db.quiz_session.id == quiz_id).select()[0].questions
			db(db.quiz_session.id == quiz_id).update(questions = y + [question_list[x].id])
			questions.append(question_list[x].question)
			option1.append(question_list[x].option_a)
			option2.append(question_list[x].option_b)
			option3.append(question_list[x].option_c)
			option4.append(question_list[x].option_d)
	else:
		questions = []
		option1 = []
		option2 = []
		option3 = []
		option4 = []
		for i in db(db.quiz_session.id == quiz_id).select()[0].questions:
			question = db(db.question_bank.id == i).select()[0]
			questions.append(question.question)
			option1.append(question.option_a)
			option2.append(question.option_b)
			option3.append(question.option_c)
			option4.append(question.option_d)			
		
	return dict(session_id = session_id,quiz_id = quiz_id,questions = questions,option1 = option1,option2 = option2,option3 = option3,option4 = option4,answers = answers)


def evaluate():
	session_id = request.args[0]
	quiz_id = request.args[1]
	answers = request.args[2]
	r = check(session_id)
	if(r == '0'):
		response.title = "hello world"
		redirect(URL('not_logged_in'))
	name = get_name(session_id)
	response.title = "Player: " + name
	score = 0
	questions = db(db.quiz_session.id == quiz_id).select()[0].questions
	db(db.quiz_session.id == quiz_id).update(status = 1)
	real_answers = []
	for i in range(0,len(questions)):
		real_answers.append(answers[2*i + 1])
		if (db(db.question_bank.id == questions[i]).select()[0].answer == answers[2*i +1]):
			score = score + 1
	db(db.quiz_session.id == quiz_id).update(given_answers = real_answers,Score = score)
	return dict(session_id = session_id,score = score)


def quiz_complete():
	response.title = "You have successfully completed the quiz"

def my_quizes():
	session_id = request.args[0]
	r = check(session_id)
	if(r == '0'):
		response.title = "hello world"
		redirect(URL('not_logged_in'))
	name = get_name(session_id)
	quizes = db(db.details.Email == name).select()[0]
	statuses = []
	scores = []
	quizes = db(db.quiz_session.Email == name).select()
	quizes_id = []
	for row in quizes:
		quizes_id.append(row.id)
		statuses.append(row.status)
		scores.append(row.Score)
	return dict(session_id = session_id,quizes_id = quizes_id,statuses = statuses,scores = scores)

def pause_quiz():
	session_id = request.args[0]
	quiz_id = request.args[1]
	answers = request.args[2]
	r = check(session_id)
	if(r == '0'):
		response.title = "hello world"
		redirect(URL('not_logged_in'))
	name = get_name(session_id)
	real_answers = []
	length = len(db(db.quiz_session.id == quiz_id).select()[0].questions)
	for i in range(0,length):
		real_answers.append(answers[2*i + 1])
	db(db.quiz_session.id == quiz_id).update(given_answers = real_answers)
	quiz = db(db.quiz_session.id == quiz_id).update(status = 2)
	redirect(URL('my_quizes/'+session_id))
	return dict()


def quiz_stats():
	session_id = request.args[0]
	r = check(session_id)
	if(r == '0'):
		response.title = "hello world"
		redirect(URL('not_logged_in'))
	name = get_name(session_id)
	quiz_id = request.args[1]
	quiz_status = request.args[2]
	response.title = quiz_id
	quiz = db(db.quiz_session.id == quiz_id).select()[0]
	question_ids = quiz.questions
	questions = []
	answers = []
	given_answers = db(db.quiz_session.id == quiz_id).select()[0].given_answers
	your_answers = []
	for i in range(0,len(question_ids)):
		question = db(db.question_bank.id == question_ids[i]).select()[0]
		questions.append(question.question)
		'''your_answers.append(given_answers[i])
		answers.append(given_answers[i])'''
		if(given_answers[i] == '0'):
			your_answers.append("Not Attempted")
		if(given_answers[i] == 'a'):
			your_answers.append(question.option_a)
		elif(given_answers[i] == 'b'):
			your_answers.append(question.option_b)
		elif(given_answers[i] == 'c'):
			your_answers.append(question.option_c)
		elif(given_answers[i] == 'd'):
			your_answers.append(question.option_d)
		if(quiz_status == '2'):
			answers.append("Quiz Incomplete")
		elif(quiz_status == '1'):
			if(question.answer == 'a'):
				answers.append(question.option_a)
			elif(question.answer == 'b'):
				answers.append(question.option_b)
			elif(question.answer == 'c'):
				answers.append(question.option_c)
			elif(question.answer == 'd'):
				answers.append(question.option_d)
	return dict(session_id = session_id,quiz_id = quiz_id,questions = questions,answers = answers,given_answers = given_answers,your_answers = your_answers,status = quiz_status)

def logout():
	session_id = request.args[0]
	db(db.login.session_id == session_id).update(session_status = 0)
	redirect(URL('login'))	
	return dict()

def not_logged_in():
	return dict()
